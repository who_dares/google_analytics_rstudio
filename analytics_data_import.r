#Data Import Scripts for Google Analytics

#Load & Execute start_up_scripts.r

#Load Google Analytics API R Integration and Visualisation Packages. 
#Ensure Google AuthR package also enabled.

library(googleAnalyticsR) #Google Analytics API access
library(ggplot2) #R Studio Graphing Engine
library(tidyverse) #R Studio Essential Dependencies
library(forecast) #Forecasting Engine
library(tidyquant) #Numerical analysis tool
library(timetk) #Time series analyser
library(sweep) #Data frame analysis tool for forecasts
library(dplyr) #Data Wrangling Tool


#Select analytics property and create variable. Amend as appropriate. NOTE: must be the ViewId entered below as an integer.

account_name<- #enter account viewId

#set variables for date range.  Amend as approprtiate. Date can be set as YYYY-MM-DD

start_date <- "2018-01-01"
end_date<- "2018-06-09"


#PAGE VIEW DATA IMPORT SCRIPTS

#Retrieves page views data and store as data frame.

page_views_df_main <- google_analytics(account_name, 
                          date_range = c(start_date, end_date),
                          metrics = c("pageviews"),
                          dimensions = c("date"))

#Retrieves page view and day of week data and stores and data frame.

page_views_dow <- google_analytics(account_name, 
                            date_range = c(start_date, end_date),
                            metrics = c("pageviews"), 
                            dimensions = c("dayOfWeekName"),
                            max = 5000)

#SESSIONS DATA IMPORT SCRIPTS


#Retrieve session data and store as data frame.

session_df_main <- google_analytics(account_name, 
                                      date_range = c(start_date, end_date),
                                      metrics = c("sessions"),
                                      dimensions = c("date"))

#Retrieves session and day of week data and stores as data frame.

sessions_dow <- google_analytics(account_name, 
                                   date_range = c(start_date, end_date),
                                   metrics = c("sessions"), 
                                   dimensions = c("dayofWeekName"),
                                   max = 5000)

#Retrieves session and source/medium and date data and stores as data frame.

sessions_medium <- google_analytics(account_name, 
                                date_range = c(start_date, end_date),
                                metrics = c("sessions"), 
                                dimensions = c("medium", "date"),
                                max = 5000)

#Retrieves session and location data and stores as data frame.

sessions_location <- google_analytics(account_name, 
                                    date_range = c(start_date, end_date),
                                    metrics = c("sessions"), 
                                    dimensions = c("country"),
                                    max = 5000)

#Filter to top 20 locations by session

sessions_location_top20 <- sessions_location %>%
  filter(rank(desc(sessions))<=20)


#Sessions Data Frame Grouped By Medium

sessions_medium_grouped <- sessions_medium %>%
  group_by(medium)%>%
  summarise(sum_sessions = sum(sessions, na.rm = TRUE))

#Retrieves session, bounce rate, source/medium and date data and stores as data frame.

sessions_br_medium <- google_analytics(account_name, 
                                    date_range = c(start_date, end_date),
                                    metrics = c("sessions", "bounceRate"), 
                                    dimensions = c("medium", "date"),
                                    max = 5000)

#Retrieves goal completions, medium and date data and stores as data frame.

goal_completion_medium_df <- google_analytics(account_name, 
                                       date_range = c(start_date, end_date),
                                       metrics = c("goalCompletionsAll"), 
                                       dimensions = c("medium", "date"),
                                       max = 5000)

#Group goal completions by medium

goalcomps_medium_grouped <- goal_completion_medium_df %>%
  group_by(medium)%>%
  summarise(sum_goals = sum(goalCompletionsAll, na.rm = TRUE))

#Goal Completions

goal_completions <- google_analytics(account_name,
                                     date_range = c("2018-03-15", end_date),
                                     metrics = c("goalCompletionsAll"),
                                     dimensions = c("date"))

#Goal Completions by Medium & Location

goal_completions_medloc <- google_analytics(account_name,
                                     date_range = c("2018-03-15", end_date),
                                     metrics = c("goalCompletionsAll"),
                                     dimensions = c("date", "medium", "continentId"))

goalcompmedloc_subset <- subset(goal_completions_medloc, goalCompletionsAll >= 1)

#Location Pageviews

location_pageviews <-google_analytics(account_name,
                                      date_range = c(start_date, end_date),
                                      metrics = c("pageviews"),
                                      dimensions =c("country"))


#Basic summary operation function. Insert data frame vector name between brackets. Outputs all summary stats (average, mean, min, max etc...)

summary()