CPanel Commit

#Load Google Analytics API R Integration and Visualisation Packages. 
#Ensure Google AuthR package also enabled.
library(googleAnalyticsR) #Google Analytics API access
library(ggplot2) #R Studio Graphing Engine
library(tidyverse) #R Studio Essential Dependencies
library(forecast) #Forecasting Engine
library(tidyquant) #Numerical analysis tool
library(timetk) #Time series analyser
library(sweep) #Data frame analysis tool for forecasts
library(dplyr) #Data Wrangling Tool

#Authorize Google Analytics R- this will open a webpage
#You must be logged into your Google Analytics account on your web browser

ga_auth()

#Verify with token in console

#create account list variable

my_accounts <- ga_account_list()

#Print and retrive full account list

View(my_accounts)