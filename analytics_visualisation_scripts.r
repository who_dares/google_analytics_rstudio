#Visualisation Scripts for Google Analytics

#Load & Execute start_up_scripts.r
#Load & Execure data_import_scripts.r

#Load Google Analytics API R Integration and Visualisation Packages. 
#Ensure Google AuthR package also enabled.

library(googleAnalyticsR) #Google Analytics API access
library(ggplot2) #R Studio Graphing Engine
library(tidyverse) #R Studio Essential Dependencies
library(forecast) #Forecasting Engine
library(tidyquant) #Numerical analysis tool
library(timetk) #Time series analyser
library(sweep) #Data frame analysis tool for forecasts
library(dplyr) #Data Wrangling Tool

#GRAPHING SCRIPTS

#PAGE VIEW VISULISATION SCRIPTS

#Page Views Line Graph w/smooth trend line- Visualation Script

page_views_df_main %>%
  ggplot(aes(x=date, y=pageviews)) +
    geom_line()+
    geom_smooth()+
    labs(title="Daily Pageviews w/ smooth trend line ", x = "Date", y = "Sessions")+
    theme_tq()

#Page Views Scatter Plot- Visualisation Script

page_views_df_main %>%
  ggplot(aes(x=date, y=pageviews,color=pageviews)) + 
  geom_point() + 
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  labs(title="Daily Pageviews - Scatterplot", x = "Date", y = "Pageviews")+
  theme_tq()

#Page Views Scatter Plot w/trend line- Visualisation Script

page_views_df_main %>%
  ggplot(aes(x=date,y=pageviews, color=pageviews)) + 
  geom_line() + 
  geom_smooth(method="lm") +
  labs(title="Daily Pageviews w/ long term trend line ", x = "Date", y = "Pageviews")+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  theme_tq()

#Page view/Day of week Bar chart- Visualisation Script

page_views_dow %>%
  ggplot(aes(x = dayOfWeekName, y = pageviews, fill=pageviews)) + 
  geom_col()+
  labs(title="Page Views by Day", x = "Day", y = "Pageviews")+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))

sessions_dow %>%
  ggplot(aes(x = dayofWeekName, y = sessions, fill= sessions)) + 
  geom_col()+
  labs(title="Sessions by Day", x = "Day", y = "Sessions")+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))

#Sessions Scatter Plot w/smoothing line - Visualisation Script

session_df_main %>%
  ggplot(aes(x=date, y=sessions, color=sessions)) +
  geom_point()+
  geom_smooth()+
  labs(title="Daily Sessions w/ smooth trend line ", x = "Date", y = "Sessions")+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  theme_tq()

#Sesions Line Graph w/trend - Visualisation Script

session_df_main %>%
  ggplot(aes(x = date,y = sessions, color = sessions)) + 
  geom_line() + 
  geom_smooth(method="lm") +
  labs(title="Daily Sessions w/ long term trend line", x = "Date", y = "Sessions")+
  theme_tq()

#Sessions Medium Bar Chart - Visualisation Script

sessions_medium_grouped %>%
  ggplot(aes(reorder(x=medium, -sum_sessions), y=sum_sessions, fill=medium, label = sum_sessions))+ 
  geom_col() + 
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  labs(title = "Sessions - Traffic Source", x = "Traffic Source", y = "Sessions")+
  geom_label()+
  theme_tq()

#Ordered graph for top 20 locations by sessions

sessions_location_top20 %>%
  ggplot(aes(reorder(x = country, sessions), y=sessions, fill= country))+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  labs(title = "Sessions by Country", x = "Country", y = "Sessions")+
  geom_col()


#Goal Completions Line Plot w/trend

goal_completions %>%
  ggplot(aes(x = date, y = goalCompletionsAll)) +
  geom_line()+
  geom_smooth(method="lm")+
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  labs(title = "Goal Completions - Trend", x = "Conversions", y = "Website Conversions")+
  theme_tq()

#Goal Completions Medium Grouped

goalcomps_medium_grouped %>%
  ggplot(aes(reorder(x = medium, -sum_goals), y=sum_goals, fill=medium, label = sum_goals))+ 
  geom_col() + 
  theme(axis.text.x = element_text(angle = 90, hjust = 1))+
  labs(title = "Website Conversions - Traffic Source", x = "Traffic Source", y = "Conversions")+
  geom_label()+
  theme_tq()
